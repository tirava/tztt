package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/tirava/tztt/internal/domain/services"
	"gitlab.com/tirava/tztt/internal/storages"

	"gitlab.com/tirava/tztt/internal/http"

	log "github.com/sirupsen/logrus"
)

const waitStateWorkers = 100 * time.Millisecond

func main() {
	fileName := filepath.Base(os.Args[0])
	flag.Usage = func() {
		fmt.Printf("Start http server: %s [-level=5] [-port=8080] [-timeout=1s] [-workers=2]\n", fileName)
		flag.PrintDefaults()
	}

	httpPort := flag.String("port", "8080", "port for http-server listening")
	logLevel := flag.Uint("level", 5, "log level 0-6 (panic-trace)")
	timeout := flag.String("timeout", "1s", "timeout duration (h, s, ms, ns, ...)")
	workers := flag.Uint("workers", 4, "number of state workers (>=2)")
	flag.Parse()

	db, err := storages.NewDB(context.Background(), "inmemory", "")
	if err != nil {
		log.Fatal(err)
	}

	tt, err := time.ParseDuration(*timeout)
	if err != nil {
		log.Fatal(err)
	}

	ts := services.NewTranslationService(db, tt, *workers)

	lg := log.New()
	lg.Level = log.Level(*logLevel)
	log.Printf("Logger started at mode: %s", lg.Level)

	http.StartHTTPServer(":"+*httpPort, lg, ts)

	log.Println("Wait all workers stopping...")
	ts.CloseStateWorkers()
	time.Sleep(waitStateWorkers)
}
