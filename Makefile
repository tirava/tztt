.PHONY: build run docker-build docker-run up down restart

PORT ?= 8080
LEVEL ?= 5
TIMEOUT ?= 1s
WORKERS ?= 4

build:
	go build -o app/tztt_server cmd/tztt_server/*.go

run:
	./app/tztt_server -level=$(LEVEL) -port=$(PORT) -timeout=$(TIMEOUT) -workers=$(WORKERS)

docker-build:
	docker build -t tztt_server -f ./build/package/tztt_server/Dockerfile .

docker-run:
	docker run --rm -d -p $(PORT):$(PORT) --name tztt_server tztt_server app/tztt_server

up:
	docker-compose up -d --build

down:
	docker-compose down

restart: down up