// Package interfaces describes all interfaces.
package interfaces

import (
	"context"

	"gitlab.com/tirava/tztt/internal/domain/entities"

	"github.com/google/uuid"
)

// DB is thw main interface for any DB.
type DB interface {
	CreateTranslation(context.Context) (*entities.Translation, error)
	DeleteTranslation(context.Context, uuid.UUID) error
	GetTranslation(context.Context, uuid.UUID) (*entities.Translation, error)
	GetAllTranslations(context.Context) ([]entities.Translation, error)
	ChangeTranslationState(context.Context, uuid.UUID, entities.State) error
	GetTranslationState(context.Context, uuid.UUID) (entities.State, error)
}

// DBDefault injects for all DB implementations.
type DBDefault struct{}

// CreateTranslation adds new translation into db.
func (DBDefault) CreateTranslation(context.Context) (*entities.Translation, error) {
	panic("CreateTranslation not implemented!")
}

// DeleteTranslation mark translation deleted in db.
func (DBDefault) DeleteTranslation(context.Context, uuid.UUID) error {
	panic("DeleteTranslation not implemented!")
}

// GetTranslation returns one translation from db.
func (DBDefault) GetTranslation(context.Context, uuid.UUID) (*entities.Translation, error) {
	panic("GetTranslationByID not implemented!")
}

// GetAllTranslations returns all translations from db.
func (DBDefault) GetAllTranslations(context.Context) ([]entities.Translation, error) {
	panic("GetAllTranslations not implemented!")
}

// ChangeTranslationState changes translation state.
func (DBDefault) ChangeTranslationState(context.Context, uuid.UUID, entities.State) error {
	panic("ChangeTranslationState not implemented!")
}

// GetTranslationState returns translation state.
func (DBDefault) GetTranslationState(context.Context, uuid.UUID) (entities.State, error) {
	panic("GetTranslationState not implemented!")
}
