package entities

// Status base type.
type State int

// Status enums.
const (
	StateCreated State = iota
	StateActive
	StateInterrupted
	StateFinished
)

// String is stringer for TagType enums.
func (s State) String() string {
	return [...]string{"created", "active", "interrupted", "finished"}[s]
}
