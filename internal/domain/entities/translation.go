// Package entities implements entity models.
package entities

import (
	"time"

	"github.com/google/uuid"
)

// Translation is the base Translation's struct.
type Translation struct {
	ID      uuid.UUID
	Created time.Time
	State   State
}

// NewTranslation returns new translate struct.
func NewTranslation() *Translation {
	return &Translation{
		ID:      uuid.New(),
		Created: time.Now(),
	}
}
