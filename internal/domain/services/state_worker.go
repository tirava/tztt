package services

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/tirava/tztt/internal/domain/entities"
	"gitlab.com/tirava/tztt/internal/domain/interfaces"
)

// nolint:funlen
func stateWorker(workID uint, control <-chan *transControl) {
	log.Println("Started state worker:", workID)

	for tc := range control {
		switch {
		case tc.currState == entities.StateActive && tc.newState == entities.StateFinished:
			tc.mux.Lock()
			(*tc.lockedObjects)[tc.id] = true
			tc.mux.Unlock()
			<-time.After(tc.timeout)

			if err := changeStateWithCheck(workID, tc, tc.db, tc.currState); err != nil {
				log.Println(err)
				continue
			}

			tc.mux.Lock()
			delete(*tc.lockedObjects, tc.id)
			tc.mux.Unlock()

		case tc.currState == entities.StateActive && tc.newState == entities.StateInterrupted:
			tc.mux.Lock()
			if _, ok := (*tc.lockedObjects)[tc.id]; ok {
				tc.mux.Unlock()
				log.Printf("translate locked in worker: %d, translation id: %s, control: %s->%s",
					workID, tc.id, tc.currState, tc.newState)

				continue
			}
			tc.mux.Unlock()

			if err := changeStateWithCheck(workID, tc, tc.db, tc.currState); err != nil {
				log.Println(err)
				continue
			}

			tc.currState = entities.StateInterrupted
			tc.newState = entities.StateFinished

			<-time.After(tc.timeout)

			if err := changeStateWithCheck(workID, tc, tc.db, tc.currState); err != nil {
				log.Println(err, ", may be it was activated in other worker")
				continue
			}

		case tc.currState == entities.StateInterrupted && tc.newState == entities.StateActive:
			if err := changeStateWithCheck(workID, tc, tc.db, tc.currState); err != nil {
				log.Println(err)
				continue
			}

		default:
			log.Printf("unknown control in worker: %d, translation id: %s, control: %s->%s",
				workID, tc.id, tc.currState, tc.newState)
			continue
		}

		log.Printf("set state in worker: %d, translation id: %s, control: %s->%s",
			workID, tc.id, tc.currState, tc.newState)
	}

	log.Println("Closed state worker:", workID)
}

func changeStateWithCheck(workID uint, tc *transControl, db interfaces.DB, mustCurrState entities.State) error {
	currState, err := db.GetTranslationState(tc.ctx, tc.id)
	if err != nil {
		return fmt.Errorf("error get translation state in worker: %d, translation id: %s, error: %w",
			workID, tc.id, err)
	}

	if currState != mustCurrState {
		return fmt.Errorf("bad current state in worker: %d, translation id: %s, control: %s->%s",
			workID, tc.id, currState, tc.newState)
	}

	if err := db.ChangeTranslationState(tc.ctx, tc.id, tc.newState); err != nil {
		return fmt.Errorf("error change translation state in worker: %d, translation id: %s, error: %w",
			workID, tc.id, err)
	}

	return nil
}
