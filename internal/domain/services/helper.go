package services

import "github.com/google/uuid"

func decodeUID(tsID string) uuid.UUID {
	uid, err := uuid.Parse(tsID)
	if err != nil {
		uid = uuid.Nil
	}

	return uid
}
