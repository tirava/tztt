// Package services implements translation's service operations.
package services

import (
	"context"
	"errors"
	"log"
	"sync"
	"time"

	"github.com/google/uuid"

	"gitlab.com/tirava/tztt/internal/domain/entities"
	"gitlab.com/tirava/tztt/internal/domain/interfaces"
)

// TranslationService is the main translation service struct.
type TranslationService struct {
	db            interfaces.DB
	control       chan *transControl
	timeout       time.Duration
	lockedObjects map[uuid.UUID]bool
	mux           sync.Mutex
	wg            sync.WaitGroup
}

type transControl struct {
	ctx           context.Context
	db            interfaces.DB
	id            uuid.UUID
	timeout       time.Duration
	currState     entities.State
	newState      entities.State
	lockedObjects *map[uuid.UUID]bool
	mux           *sync.Mutex
}

// NewTranslationService inits main service fields.
// nolint:gomnd
func NewTranslationService(db interfaces.DB, timeout time.Duration, workers uint) *TranslationService {
	tc := &TranslationService{
		db:            db,
		control:       make(chan *transControl, workers),
		timeout:       timeout,
		lockedObjects: make(map[uuid.UUID]bool),
	}

	if workers < 2 {
		workers = 2
	}

	for i := uint(1); i <= workers; i++ {
		go stateWorker(i, tc.control)
	}

	return tc
}

// CreateTranslation creates new translation.
func (ts *TranslationService) CreateTranslation(ctx context.Context) (*entities.Translation, error) {
	return ts.db.CreateTranslation(ctx)
}

// ListAllTranslations gets all translation.
func (ts *TranslationService) ListAllTranslations(ctx context.Context) ([]entities.Translation, error) {
	return ts.db.GetAllTranslations(ctx)
}

// ListTranslation gets one translation.
func (ts *TranslationService) ListTranslation(ctx context.Context, tsID string) (*entities.Translation, error) {
	return ts.db.GetTranslation(ctx, decodeUID(tsID))
}

// DeleteTranslation deletes one translation.
func (ts *TranslationService) DeleteTranslation(ctx context.Context, tsID string) error {
	return ts.db.DeleteTranslation(ctx, decodeUID(tsID))
}

// ChangeTranslationState changes translation state.
func (ts *TranslationService) ChangeTranslationState(ctx context.Context, tsID string, newState entities.State) error {
	tid := decodeUID(tsID)

	currState, err := ts.db.GetTranslationState(ctx, tid)
	if err != nil {
		return err
	}

	switch {
	case currState == entities.StateCreated && newState == entities.StateActive:
		log.Println("Created -> Active", tid)
		return ts.db.ChangeTranslationState(ctx, tid, newState)
	case currState == entities.StateActive && newState == entities.StateFinished:
		log.Println("Active -> Finish", tid)
	case currState == entities.StateActive && newState == entities.StateInterrupted:
		log.Println("Active -> Interrupted", tid)
	case currState == entities.StateInterrupted && newState == entities.StateActive:
		log.Println("Interrupted -> Active", tid)
	default:
		return errors.New("unknown state control")
	}

	tc := &transControl{
		ctx:           ctx,
		db:            ts.db,
		id:            tid,
		timeout:       ts.timeout,
		currState:     currState,
		newState:      newState,
		lockedObjects: &ts.lockedObjects,
		mux:           &ts.mux,
	}

	// nolint:gomnd
	ts.wg.Add(1)

	go func() {
		defer ts.wg.Done()
		ts.control <- tc
	}()

	return nil
}

// CloseStateWorkers closes workers.
func (ts *TranslationService) CloseStateWorkers() {
	ts.wg.Wait()
	time.Sleep(ts.timeout)
	close(ts.control)
}
