package http

import (
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
)

type contextKey string

func (h handler) prepareRoutes() http.Handler {
	h.addPath("GET /hello($|/.*)", h.hello)

	h.addPath("POST /api/v1/streams$", h.create)
	h.addPath("DELETE /api/v1/streams(/.*)", h.delete)
	h.addPath("GET /api/v1/streams$", h.listAll)
	h.addPath("GET /api/v1/streams(/.*)", h.list)
	h.addPath("POST /api/v1/streams/state/activate(/.*)", h.activate)
	h.addPath("POST /api/v1/streams/state/interrupt(/.*)", h.interrupt)
	h.addPath("POST /api/v1/streams/state/finish(/.*)", h.finish)

	siteHandler := h.pathMiddleware()
	siteHandler = h.loggerMiddleware(siteHandler)
	siteHandler = h.panicMiddleware(siteHandler)

	return siteHandler
}

func (h handler) addPath(regex string, handler http.HandlerFunc) {
	h.handlers[regex] = handler
	cache, err := regexp.Compile(regex)

	if err != nil {
		log.Fatal(err)
	}

	h.cacheHandlers[regex] = cache
}
