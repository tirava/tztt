package http

import "strings"

func getIDFromURL(url string) string {
	chunks := strings.Split(url, "/")
	if len(chunks) == 0 {
		return ""
	}

	return chunks[len(chunks)-1]
}
