package http

import (
	"errors"
	"net/http"

	"github.com/google/jsonapi"

	log "github.com/sirupsen/logrus"
)

// ClientError model.
type ClientError struct {
	logger *log.Logger
}

// ErrorObject model.
type ErrorObject struct {
	ID     string                  `json:"id,omitempty"`
	Title  string                  `json:"title,omitempty"`
	Detail string                  `json:"detail,omitempty"`
	Status string                  `json:"status,omitempty"`
	Code   string                  `json:"code,omitempty"`
	Meta   *map[string]interface{} `json:"meta,omitempty"`
}

func newError(logger *log.Logger) *ClientError {
	return &ClientError{
		logger: logger,
	}
}

func (e ClientError) send(w http.ResponseWriter, code int, err error, description string) {
	if err == nil {
		err = errors.New("")
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	errMsg := &jsonapi.ErrorObject{
		Status: http.StatusText(code),
		Title:  err.Error(),
		Detail: description,
	}

	if err := jsonapi.MarshalErrors(w, []*jsonapi.ErrorObject{errMsg}); err != nil {
		e.logger.Errorf("can't encode error data: %s", err)
		return
	}
}
