package http

import (
	"net/http"

	"gitlab.com/tirava/tztt/internal/domain/entities"

	log "github.com/sirupsen/logrus"
)

func (h handler) activate(w http.ResponseWriter, r *http.Request) {
	state := entities.StateActive

	h.changeState(w, r, state)
}

func (h handler) interrupt(w http.ResponseWriter, r *http.Request) {
	state := entities.StateInterrupted

	h.changeState(w, r, state)
}

func (h handler) finish(w http.ResponseWriter, r *http.Request) {
	state := entities.StateFinished

	h.changeState(w, r, state)
}

func (h handler) changeState(w http.ResponseWriter, r *http.Request, state entities.State) {
	reqID := getRequestID(r.Context())
	tid := getIDFromURL(r.URL.Path)

	if err := h.transService.ChangeTranslationState(r.Context(), tid, state); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusNotAcceptable, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusNotAcceptable, err, "error changing translation state")

		return
	}

	w.WriteHeader(http.StatusAccepted)
	h.logger.WithFields(log.Fields{
		codeField: http.StatusAccepted, reqIDField: reqID,
	}).Infof("RESPONSE")
}
