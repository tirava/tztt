package http

import (
	"io"
	"net/http"
	"regexp"

	"github.com/google/jsonapi"

	"gitlab.com/tirava/tztt/internal/domain/services"

	log "github.com/sirupsen/logrus"
)

type handler struct {
	handlers      map[string]http.HandlerFunc
	cacheHandlers map[string]*regexp.Regexp
	logger        *log.Logger
	transService  *services.TranslationService
	error         *ClientError
}

func newHandlers(logger *log.Logger, ts *services.TranslationService) *handler {
	return &handler{
		handlers:      make(map[string]http.HandlerFunc),
		cacheHandlers: make(map[string]*regexp.Regexp),
		logger:        logger,
		transService:  ts,
		error:         newError(logger),
	}
}

func (h handler) hello(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	name := query.Get("name")

	if name == "" {
		name = "Nya-nya"
	}

	s := "Hello, my name is " + name

	if _, err := io.WriteString(w, s); err != nil {
		h.logger.Errorf("[hello] error write to response writer")
	}

	h.logger.WithFields(log.Fields{
		codeField: http.StatusOK, reqIDField: getRequestID(r.Context()),
	}).Infof("RESPONSE")
}

func (h handler) create(w http.ResponseWriter, r *http.Request) {
	reqID := getRequestID(r.Context())

	ts, err := h.transService.CreateTranslation(r.Context())
	if err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusInternalServerError, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusInternalServerError, err, "error create new translation")

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	tsDTO := newTranslationDTO(ts)
	if err := jsonapi.MarshalPayloadWithoutIncluded(w, tsDTO); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusInternalServerError, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusInternalServerError, err, "error while parse new translation fields")

		return
	}

	h.logger.WithFields(log.Fields{
		codeField: http.StatusCreated, reqIDField: reqID,
	}).Infof("RESPONSE")
}

func (h handler) delete(w http.ResponseWriter, r *http.Request) {
	reqID := getRequestID(r.Context())
	tid := getIDFromURL(r.URL.Path)

	if err := h.transService.DeleteTranslation(r.Context(), tid); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusNotFound, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusNotFound, err, "error delete translation")

		return
	}

	w.WriteHeader(http.StatusNoContent)
	h.logger.WithFields(log.Fields{
		codeField: http.StatusNoContent, reqIDField: reqID,
	}).Infof("RESPONSE")
}

func (h handler) listAll(w http.ResponseWriter, r *http.Request) {
	reqID := getRequestID(r.Context())

	tsAll, err := h.transService.ListAllTranslations(r.Context())
	if err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusInternalServerError, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusInternalServerError, err, "error list all translations")

		return
	}

	w.Header().Set("Content-Type", "application/json")

	tsDTOSlice := newTranslationDTOSlice(tsAll)
	if err := jsonapi.MarshalPayloadWithoutIncluded(w, tsDTOSlice); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusInternalServerError, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusInternalServerError, err,
			"error while parse new translation's array fields")

		return
	}

	h.logger.WithFields(log.Fields{
		codeField: http.StatusOK, reqIDField: reqID,
	}).Infof("RESPONSE")
}
func (h handler) list(w http.ResponseWriter, r *http.Request) {
	reqID := getRequestID(r.Context())

	tid := getIDFromURL(r.URL.Path)

	ts, err := h.transService.ListTranslation(r.Context(), tid)
	if err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusNotFound, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusNotFound, err, "error list translation")

		return
	}

	w.Header().Set("Content-Type", "application/json")

	tsDTO := newTranslationDTO(ts)
	if err := jsonapi.MarshalPayloadWithoutIncluded(w, tsDTO); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusInternalServerError, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusInternalServerError, err, "error while parse translation fields")

		return
	}

	h.logger.WithFields(log.Fields{
		codeField: http.StatusOK, reqIDField: reqID,
	}).Infof("RESPONSE")
}
