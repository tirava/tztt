package http

import (
	"time"

	"gitlab.com/tirava/tztt/internal/domain/entities"
)

type translation struct {
	ID      string `jsonapi:"primary,stream"`
	Created string `jsonapi:"attr,created"`
	State   string `jsonapi:"attr,state"`
}

func newTranslationDTO(ts *entities.Translation) *translation {
	return &translation{
		ID:      ts.ID.String(),
		Created: ts.Created.Format(time.RFC3339),
		State:   ts.State.String(),
	}
}

func newTranslationDTOSlice(tSlice []entities.Translation) []*translation {
	ts := make([]*translation, 0, len(tSlice))
	for _, t := range tSlice {
		ts = append(ts, &translation{
			ID:      t.ID.String(),
			Created: t.Created.Format(time.RFC3339),
			State:   t.State.String(),
		})
	}

	return ts
}
