// Package storages implements storage interface.
package storages

import (
	"context"
	"fmt"

	"gitlab.com/tirava/tztt/internal/domain/interfaces"
	"gitlab.com/tirava/tztt/internal/storages/inmemory"
)

// NewDB returns DB by db type.
func NewDB(ctx context.Context, dbType, dsn string) (interfaces.DB, error) {
	switch dbType {
	case "inmemory":
		return inmemory.NewMapDB()
	default:
		return nil, fmt.Errorf("incorrect storage db type name: %s", dbType)
	}
}
