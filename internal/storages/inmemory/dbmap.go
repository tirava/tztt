// Package inmemory implements memory map DB interface.
package inmemory

import (
	"context"
	"errors"
	"sync"

	"gitlab.com/tirava/tztt/internal/domain/interfaces"

	"gitlab.com/tirava/tztt/internal/domain/entities"

	"github.com/google/uuid"
)

// DBMap is the base struct for using map db.
type DBMap struct {
	interfaces.DBDefault
	sync.RWMutex
	translations map[uuid.UUID]*entities.Translation
}

// NewMapDB returns new map db storage.
func NewMapDB() (*DBMap, error) {
	return &DBMap{
		translations: make(map[uuid.UUID]*entities.Translation),
	}, nil
}

// CreateTranslation adds new translation into map db.
func (db *DBMap) CreateTranslation(ctx context.Context) (*entities.Translation, error) {
	db.Lock()
	defer db.Unlock()

	ts := entities.NewTranslation()

	db.translations[ts.ID] = ts

	return ts, nil
}

// DeleteTranslation mark translation deleted in map db.
func (db *DBMap) DeleteTranslation(ctx context.Context, id uuid.UUID) error {
	db.Lock()
	defer db.Unlock()

	if _, ok := db.translations[id]; !ok {
		return errors.New("translation not found")
	}

	delete(db.translations, id)

	return nil
}

// GetTranslation returns one translation from map db.
func (db *DBMap) GetTranslation(ctx context.Context, id uuid.UUID) (*entities.Translation, error) {
	db.Lock()
	defer db.Unlock()

	if _, ok := db.translations[id]; !ok {
		return nil, errors.New("translation not found")
	}

	return db.translations[id], nil
}

// GetAllTranslations returns all translations from map db.
func (db *DBMap) GetAllTranslations(ctx context.Context) ([]entities.Translation, error) {
	result := make([]entities.Translation, 0, len(db.translations))

	db.Lock()
	defer db.Unlock()

	for _, ts := range db.translations {
		result = append(result, *ts)
	}

	return result, nil
}

// ChangeTranslationState changes translation state in map DB.
func (db *DBMap) ChangeTranslationState(ctx context.Context, id uuid.UUID, state entities.State) error {
	db.Lock()
	defer db.Unlock()

	if _, ok := db.translations[id]; !ok {
		return errors.New("translation not found")
	}

	db.translations[id].State = state

	return nil
}

// ChangeTranslationState changes translation state in map DB.
func (db *DBMap) GetTranslationState(ctx context.Context, id uuid.UUID) (entities.State, error) {
	db.Lock()
	defer db.Unlock()

	if _, ok := db.translations[id]; !ok {
		return -1, errors.New("translation not found")
	}

	return db.translations[id].State, nil
}
