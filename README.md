# Сервер для управления состоянием объекта “Трансляция”

### Требования к сервису
См. документ [Требования](Requirements.md)

### Запуск из командной строки
Сборка:  
***make build***  
Запуск с дефолтными параметрами:  
***make run***  
или  
***./app/tztt_server***  
Запуск с кастомными параметрами:  
***PORT=8080  LEVEL=6  TIMEOUT=1s  WORKERS=4 make run***  
или  
***./app/tztt_server -level=6 -port=8080 -timeout=1s -workers=4***  
где:
 - *port* - порт прослушивания HTTP-сервера, дефолт - 8080
 - *level* - уровень логгирования 0-6 (panic-trace), дефолт - 5 (debug)
 - *timeout* - таймаут перехода между состояниями трансляции (h, s, ms, ns, ...), дефолт - 1s
 - *workers* - количество воркеров для конкурентной обработки запросов (не менее 2-х), дефолт - 4

### Запуск в докере
***make up***  
Параметры передаются через переменные окружения или через файл *.env* (в корне проекта).  
***make down*** - остановка

### API
Сервис по умолчанию будет доступен на [http://localhost:8080](http://localhost:8080)
1. Создание трансляции - POST [http://localhost:8080/api/v1/streams](http://localhost:8080/api/v1/streams)  
В ответе придет созданный объект, например:

   ```
   {
        "data": {
            "type": "stream",
            "id": "5a392df6-9153-4bd2-9c37-a928139b9ad7",
            "attributes": {
                "created": "2020-06-14T08:36:45Z",
                "state": "created"
            }
        }
    }
   ```

2. Активация трансляции - POST [http://localhost:8080/api/v1/streams/state/activate/{id}](http://localhost:8080/api/v1/streams/state/activate/{id})
3. Прерывание трансляции - POST [http://localhost:8080/api/v1/streams/state/interrupt/{id}](http://localhost:8080/api/v1/streams/state/interrupt/{id})
После прерывания, трансляция завершится через заданный таймаут, либо ее можно будет заново активировать до истечения таймаута.
4. Завершение трансляции - POST [http://localhost:8080/api/v1/streams/state/finish/{id}](http://localhost:8080/api/v1/streams/state/finish/{id})
Трансляция завершается в течение заданного таймаута, и её нельзя заново активировать в это время.
5. Получить список всех трансляций - GET [http://localhost:8080/api/v1/streams](http://localhost:8080/api/v1/streams)
6. Получить данные одной трансляции - GET [http://localhost:8080/api/v1/streams/{id}](http://localhost:8080/api/v1/streams/{id})
7. Удалить трансляцию - DELETE [http://localhost:8080/api/v1/streams/{id}](http://localhost:8080/api/v1/streams/{id})

### Тестирование
Для упрощения тестирования в директории *./tests* находится тестовая коллекция для Postman-a или Newman-a   
Например:  
***newman run ./tests/TZTT_postman_collection.json -n 10***
